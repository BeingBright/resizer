Resizer = {}

Resizer.ToolButton = {
    name = "Resize Tool",
    icon = HBU.GetBuilderLuaFolder() .. "/Vehicle/Resizer/Resizer.png",
    tooltip = "Resize Tool [LeftShift + 3]",
    bigTooltip = [[<b>Resize Tool:</b> for all you scaling needs.]],
    hoversound = "click_blip",
    clicksound = "affirm_blip_1",
    imageLayout = {"dualcolor", true},
    layout = {
        "shortcut",
        {
            key1 = KeyCode.Alpha3,
            useShift = true,
            func = function()
                Builder.SelectTool(Resizer)
            end
        }
    },
    shortcut = nil
}

Resizer.version = "V1.1"

function Resizer:Start()
    print("Resizer:Start()")
    --register the tool with builder
    if Builder and Builder.RegisterTool then
        Builder.RegisterTool(self, self.ToolButtonConfig)
    end
end

function Resizer:OnDestroy()
    --unregister the tool
    print("Resizer:OnDestroy()")
    if Builder and Builder.UnRegisterTool then
        Builder.UnRegisterTool(self, self.ToolButtonConfig)
    end
    HBBuilder.Builder.StopScaleAxis()
end

function Resizer:OnDisableTool()
    HBBuilder.Builder.StopScaleAxis()
end

function Resizer:OnEnableTool()
end

function Resizer:Update()
    if self and self.enabled == false then
        HBBuilder.Builder.StopScaleAxis()
    end

    if not self or not self.enabled then
        return
    end

    local selection = false
    local selectionChanged = false
    local pos = Vector3.zero
    local rot = Quaternion.identity
    local objSelected = {}

    if not Slua.IsNull(HBBuilder.Builder.selection) then
        for sel in Slua.iter(HBBuilder.Builder.selection) do
            if sel and not Slua.IsNull(sel) then
                if not sel:GetComponentInChildren("HBBuilder.Adjustable") then
                    table.insert(objSelected, sel)
                end
            end
        end
    end

    HBBuilder.Builder.Select(nil, "")

    for k, part in pairs(objSelected) do
        if part and not Slua.IsNull(part) then
            selection = true
            HBBuilder.Builder.Select(part.gameObject, "+")

            if self.partID ~= part:GetInstanceID() then
                self.partID = part:GetInstanceID()
                selectionChanged = true
            end

            local partContainer = part:GetComponent("PartContainer")

            if not Grid or not Grid.Settings or not Grid.Settings.useCogAsCenter then
                pos = part.transform:TransformPoint(partContainer.bounds.center)
            else
                pos =
                    part.transform:TransformPoint(
                    Vector3(partContainer.cog.x, partContainer.cog.y, partContainer.cog.z)
                )
            end
            rot = part.transform.rotation
        end
    end

    if selection then
        HBBuilder.Builder.StartScaleAxis(pos, rot, HBBuilder.Builder.selection, HBBuilder.Builder.root, true)
        if Input.GetMouseButtonUp(0) and not selectionChanged then
            if Symmetry and Symmetry.MirrorParts then
                Symmetry:MirrorParts(HBBuilder.Builder.selection)
            end
            HBBuilder.Builder.ChangedCurrentAssembly()
        end
    else
        HBBuilder.Builder.StopScaleAxis()
    end
end

return Resizer
